import java.util.Scanner;
import java.util.Random;

public class Nine {
    public static void main(String[] args) {
        String ready;
        Random rnd = new Random();
        int roll_one;
        int roll_two;
        int userSum = 0;
        int computerSum = 0;

        Scanner input = new Scanner(System.in);

        // The user's turn
        System.out.println("Ready to play? (y/n) ");
        ready = input.nextLine();

        if (!ready.equals("y")) {
            System.out.println("I think you meant yes.. Let's go!");
        }

        roll_one = rnd.nextInt(7);
        System.out.println("You rolled " + roll_one);

        System.out.println("Would you like to roll again? (y/n) ");
        ready = input.nextLine();

        if (ready.equals("y")) {
            roll_two = rnd.nextInt(7);
            userSum = (roll_one + roll_two);
            System.out.println("You rolled " + roll_two + ", in total you have: " + userSum);
        } else {
            userSum = roll_one;
        }

        // The computer's turn
        roll_one = rnd.nextInt(7);
        System.out.println("The computer rolled " + roll_one);
        
        if (roll_one <= 4) {
            roll_two = rnd.nextInt(7);
            computerSum = (roll_one + roll_two);
            System.out.println("The computer rolls again and gets " + roll_two + ", in total: " + computerSum);
        } else {
            computerSum = roll_one;
        }

    
        if (userSum > 9 && computerSum > 9) {
            System.out.println("Neither of you won..");
        } else if (userSum == computerSum) {
            System.out.println("Both of you won, party!");
        } else if (userSum > 9 || userSum < computerSum && computerSum < 10) {
            System.out.println("The computer won!");
        } else {
            System.out.println("You won!");
        }

        input.close();
    }
}
