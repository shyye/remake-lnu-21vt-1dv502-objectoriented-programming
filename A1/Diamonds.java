import java.util.Scanner;

public class Diamonds {
    public static void main(String[] args) {
        int row;
        int stars = 1;
        Scanner userInput = new Scanner(System.in);

        do {  
            System.out.println("Give a positive number: ");
            row = userInput.nextInt();

            // rows
            for (int i = 0; i < row; i++) {
                
                // inner loop spaces before
                for (int j = 1; j < (row - i); j++) {
                    System.out.print(" ");
                }

                // inner loop stars
                for (int j = 0; j < stars; j++) {
                    System.out.print("*"); // should maybe be char instead????
                }

                System.out.println();
                stars += 2; 
            }

            stars -= 4;

            for (int i = (row-1); i > 0; i--) {
                
                // inner loop spaces before
                for (int j = 0; j < (row - i); j++) {
                    System.out.print(" ");
                }

                // inner loop stars
                for (int j = 0; j < stars; j++) {
                    System.out.print("*"); // should maybe be char instead????
                }

                System.out.println();
                stars -= 2; 
            }

            //restore star value
            stars = 1;

        } while(row > 0);

        System.out.println("End of program.");

        userInput.close();
    }   
}
