import java.util.Scanner;

public class DangerousWork {
    public static void main(String[] args) {
        double desiredSalary;
        double salary = 0.01;
        int days = 1;

        Scanner userInput = new Scanner(System.in);
        System.out.println("How much would you like to earn? (if not a whole number, use comma(,) ) ");
        desiredSalary = userInput.nextDouble();
        userInput.close();

        while (salary <= desiredSalary) {
            salary = salary * 2;
            days++; 
        }

        // if days = 27, salary =   671 088.64
        // if days = 28, salary = 1 342 177.28   Inte samma antal dagar som i facit/exemplet i assignment men rätt enligt lönen

        // Recommend to rest if too many (30) work days
        if (days >= 30) {
            System.out.println("It will take " + days + " days, but I think you should go home and play video games instead..");
        } else {
            System.out.println("You will have your money in " + days + " days. (" + days + " days = " + salary + ")");
        }  
    }
}

