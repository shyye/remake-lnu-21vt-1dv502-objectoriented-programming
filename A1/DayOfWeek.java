import java.util.Scanner;

public class DayOfWeek {
    public static void main(String[] args) {
        String weekDay = "";
        int h; // day of week where 0 is Saturday, 1 is Sunday and so on.
        int q; // day of the month.
        int m; // the month, where 3 is March, 4 is April. January and February are counted as months 13 and 14 for the previous year.
        int j; // year / 100
        int k; // year of the century (year % 100)
        int year;
        Scanner userInput = new Scanner(System.in);

        // Inputs
        System.out.print("Enter a year: ");
        year = userInput.nextInt();

        System.out.print("Enter month (1-12): ");
        m = userInput.nextInt();

        System.out.print("Enter day of the month (1-31): ");
        q = userInput.nextInt();

        userInput.close();
      
        // Formula
        j = year / 100;
        k = year % 100;

        if (m == 1) {
            m = 13;
            k -=1;
        } else if (m == 2) {
            m = 14;
            k -= 1;
        }

        h = ( q + ((26*(m+1)) / 10) + k + (k/4) + (j/4) + (5*j) ) % 7;

        switch (h) {
            case 0:
                weekDay = "Saturday";
                break;
            case 1:
                weekDay = "Sunday";
                break;
            case 2:
                weekDay = "Monday";
                break;
            case 3:
                weekDay = "Tuesday";
                break;
            case 4:
                weekDay = "Wednesday";
                break;
            case 5:
                weekDay = "Thursday";
                break;
            case 6:
                weekDay = "Friday";
                break;
            default:
                // default
        }

        System.out.println("Day of week is " + weekDay);
    }
}
