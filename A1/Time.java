import java.util.Scanner;

public class Time {

    public static void main(String[] args) {
        int userInput;
        int minutes;
        int hours;
        int seconds;

        Scanner scanner = new Scanner(System.in);

        // Ask for input
        System.out.print("Give a number of seconds: ");
        userInput = scanner.nextInt();

        // 1h = 60 min = 3600 sec
        hours = userInput / 3600;
        minutes = (userInput % 3600) / 60;
        seconds = (userInput % 3600) % 60;

        // Present answer
        System.out.print(seconds + " corresponds to: " + hours + " hours, " + minutes + " minutes, " + seconds + " seconds.");

        scanner.close();
    }

}
