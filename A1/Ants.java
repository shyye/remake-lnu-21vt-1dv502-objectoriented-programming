import java.util.ArrayList;
import java.util.Random;

public class Ants {
    public static void main(String[] args) {
        Random rnd = new Random();
        int startPosition;
        int position;
        int nextStep;
        int sum = 0;
        int countSteps = 0;
        int[][] chessboard = {
            {1, 2, 3, 4, 5, 6, 7, 8},
            {9, 10, 11, 12, 13, 14, 15, 16},
            {17, 18, 19, 20, 21, 22, 23, 24},
            {25, 26, 27, 28, 29, 30, 31, 32},
            {33, 34, 35, 36, 37, 38, 39, 40},
            {41, 42, 43, 44, 45, 46, 47, 48},
            {49, 50, 51, 52, 53, 54, 55, 56},
            {57, 58, 59, 60, 61, 62, 63, 64}
        };
        ArrayList<Integer> visitedSquares = new ArrayList<>();
        ArrayList<Integer> average = new ArrayList<>();

        // run whole sequence 10 times, maybe #cat comment don't know
        for (int i = 1; i < 11; i++) {

            // start position
            int row = rnd.nextInt(8);
            int column = rnd.nextInt(8);

            startPosition = chessboard[row][column];
            visitedSquares.add(startPosition);

            countSteps = 1;

            // The ant walks until all squares are visited
            while (visitedSquares.size() <= 63) {

                // nextStep
                // 0 = move up
                // 1 = move left
                // 2 = move down
                // 3 = move right

                nextStep = rnd.nextInt(4);
                countSteps += 1;

                // Ant can't go outside edge
                // (some values are hardcoded, not totally random, could be improved)
                if (row == 0 && nextStep == 0) {
                    if (column == 0) {
                        nextStep = 3; // move right
                    } else if (column == 7) {
                        nextStep = 1; // move left
                    } else {
                        nextStep = rnd.nextInt(2)+1; // random direction 1 (left) or 2 (down)
                    }
                }
                if (row == 7 && nextStep == 2) {
                    if (column == 0) {
                        nextStep = 3; // move right
                    } else if (column == 7) {
                        nextStep = 1; // move left
                    } else {
                        nextStep = rnd.nextInt(2); //random direction between 0 (up) or 1 (left)
                    }
                }
                if (column == 0 && nextStep == 1) {
                    nextStep = 3; // move right
                }
                if (column == 7 && nextStep == 3) {
                    nextStep = 1; // move left
                }

                // take next step
                switch (nextStep) {
                    case 0:
                        row -= 1;
                        break;
                    case 1:
                        column -= 1;
                        break;
                    case 2:
                        row += 1;
                        break;
                    case 3:
                        column += 1;
                        break;
                    default:
                        System.out.println("Something went wrong with sitch nextStep");
                }

                position = chessboard[row][column];
                

                // Register visited squares  // TODO: Note the best solution
                boolean check = true;
                for (int v: visitedSquares) {
                    if (v == position) {
                        check = false;
                    } else {
                        // do nothing
                    }
                }

                if (check) {
                    visitedSquares.add(position);
                }
            }

            System.out.println("Number of steps in simulation " + i + ": " + countSteps);
            average.add(countSteps);  

            // restore values
            visitedSquares.clear();
        }
      
        for (int a: average) {
            sum += a;
        }

        int averageSum = sum / 10; // Hardcoded "10" maybe not good
        System.out.println("Average amount of steps: " + averageSum);        
    }
}
