import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Hex2Dec {

    public static int hexToDecimal(String hex) {
        // För enkelt men fungerar: int decimalHex = Integer.parseInt(hex, 16);
        // resource: https://www.javatpoint.com/java-hex-to-decimal   ^

        int base = 16; // base 16 (hex)
        int exponent = hex.length() - 1;
        System.out.println("exponent: "+ exponent);
        //char character;
        int sum = 0;

        hex = hex.toUpperCase();
        
        // {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        ArrayList<Character> hexArray = new ArrayList<>(Arrays.asList('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F')); 
        
        // Get hex-value from index in array
        for(int i = 0; i < hex.length(); i++){
            char character = hex.charAt(i);
            int value = hexArray.indexOf(character);
  
            sum += value * Math.pow(base,exponent);
            exponent--;
        }
        return sum;
    }

    public static void main(String[] args) {
        String stringHex;
        
        Scanner userInput = new Scanner(System.in);
        System.out.print("Enter a hex number: ");
        stringHex = userInput.nextLine();
        userInput.close();
        
        System.out.print(hexToDecimal(stringHex));      
    }
}
