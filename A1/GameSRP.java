import java.util.Random;
import java.util.Scanner;

public class GameSRP {
    public static void main(String[] args) {
        int userValue;
        Scanner userInput = new Scanner(System.in);
        Random rnd = new Random();
        
        do {
            // User's turn
            System.out.print("Scissor (1), rock (2), paper (3) or 0 to quit: ");
            userValue = userInput.nextInt();

            // The computer is playing numbers between 1-3
            int computerValue = rnd.nextInt(3) + 1;

            if (userValue == computerValue) {
                System.out.println("It's a draw!\n");
            } else if (userValue == 1) {
                if (computerValue == 3) {
                    System.out.println("You won, computer had paper!\n");
                } else {
                    System.out.println("You lost, the computer had rock!\n");
                }
            } else if (userValue == 2) {
                if (computerValue == 1) {
                    System.out.println("You won, computer had scissor!\n");
                } else {
                    System.out.println("You lost, the computer had paper!\n");
                }
            } else if (userValue == 3) {
                if (computerValue == 2) {
                    System.out.println("You won, computer had rock!\n");
                } else {
                    System.out.println("You lost, the computer had scissor!\n");
                }
            }
            
        } while (userValue != 0);

        userInput.close();

        System.out.println("You ended the game.");
    }
}
