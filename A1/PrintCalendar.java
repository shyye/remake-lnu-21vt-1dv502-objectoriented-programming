import java.util.Scanner;

// TODO: Fix this, not good solution

public class PrintCalendar {

    public static String dayOfWeek(int year, int month) {
        int h; // day of week where 0 is Saturday, 1 is Sunday and so on.
        int q = 1; // day of the month / first day in month
        int m = month; // the month, where 3 is March, 4 is April. January and February are counted as months 13 and 14 for the previous year.
        int j; // year / 100
        int k; // year of the century (year % 100)
        String weekDay = "";  // OBS är detta fel??? definiera en string är = new string hm
        
        // Formula
        j = year / 100;
        k = year % 100;

        if (m == 1) {
            m = 13;
            k -=1;
        } else if (m == 2) {
            m = 14;
            k -= 1;
        }

        h = ( q + ((26*(m+1)) / 10) + k + (k/4) + (j/4) + (5*j) ) % 7;

        switch (h) {
            case 0:
                weekDay = "Saturday";
                break;
            case 1:
                weekDay = "Sunday";
                break;
            case 2:
                weekDay = "Monday";
                break;
            case 3:
                weekDay = "Tuesday";
                break;
            case 4:
                weekDay = "Wednesday";
                break;
            case 5:
                weekDay = "Thursday";
                break;
            case 6:
                weekDay = "Friday";
                break;
            default:
                // default
        }

        return weekDay;
    }

    // Count (will handle leap year)
    public static int numOfDaysInMonth (int year, int month) {
        int numOfDays = 0;
        switch(month) {
            case 1:
                numOfDays = 31;
                break;
            case 2:
                // Leap year
                // resource: https://sv.wikipedia.org/wiki/Skott%C3%A5r
                // REQ: be able to calculate for example leap year and so on. 
                if (year % 100 == 0 && year % 4 == 0 && year % 400 != 0) {
                } else if (year % 4 == 0) {
                    numOfDays = 29;
                } else {
                    numOfDays = 28;
                }
                break;
            case 3:
                numOfDays = 31;
                break;
            case 4:
                numOfDays = 30;
                break;
            case 5:
                numOfDays = 31;
                break;
            case 6:
                numOfDays = 30;
                break;
            case 7:
                numOfDays = 31;
                break;
            case 8:
                numOfDays = 31;
                break;
            case 9:
                numOfDays = 30;
                break;
            case 10:
                numOfDays = 31;
                break;
            case 11:
                numOfDays = 30;
                break;
            case 12:
                numOfDays = 31;
                break;
            default:
                System.out.println("Invalid number for month.");
        }
        return numOfDays;
    }

    // Returns index of weekday
    // week starts on Monday
    public static int indexOfWeekDay (String day) {
        int index = 7;

        switch (day) {
            case "Monday":
                index = 0;
                break;
            case "Tuesday":
                index = 1;
                break;
            case "Wednesday":
                index = 2;
                break;
            case "Thursday":
                index = 3;
                break;
            case "Friday":
                index = 4;
                break;
            case "Saturday":
                index = 5;
                break;
            case "Sunday":
                index = 6;
                break;
            default:
                // default
        }
        return index;
    }

    // A bit messy, sorry :( Could be better
    public static void printCalender(int daysInMonths, int indexFirstDayOfMonth) {
        
        double weeksInMonth = daysInMonths/7.0;
        int maxRow = (int)Math.ceil(weeksInMonth);
        int day = 0;
        int dayOfMonth = 1;

        System.out.println("Mon\tTue\tWed\tThu\tFri\tSat\tSun");
        System.out.println("---------------------------------------------------");

    
        for (int row = 0; row < maxRow; row++) {
            if (row == 0) {
                for (int i = 0; i < indexFirstDayOfMonth; i++) {
                    System.out.print(" \t");
                }
                day = indexFirstDayOfMonth;
            } else {
                day = 0;
            }
            for (int i = day; i < 7; i++) {
                if (dayOfMonth <= daysInMonths) {
                    System.out.print(dayOfMonth + "\t");
                }
                dayOfMonth += 1;
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        int year;
        int month;

        // Input
        System.out.println("Enter a year after 1800: ");
        year = scan.nextInt();

        System.out.println("Enter a month (1-12): ");
        month = scan.nextInt();

        int daysInMonth = numOfDaysInMonth(year, month);

        String day = dayOfWeek(year, month);
        int indexOfDay = indexOfWeekDay(day);
        
        printCalender(daysInMonth, indexOfDay);

        scan.close();
    }
}
