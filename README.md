# LNU 21VT - 1DV502 - Objectoriented Programming
Remake(small fixes) of assignments in a programming course.  

Course: 1DV502/1DT904 - Objectoriented programming (7.5 credits)  
Semester: Spring 2021   
University: Linnaeus University, Kalmar


## Assignment 1
### Hello.java
HelloWorld, check if setup works.

### Time.java
Take user input in seconds, return corresponding time in hours, minutes and seconds. E.g.:
```
Give a number of seconds: 9999
This corresponds to: 2 hours, 46 minutes and 39 seconds.
```

### Nine.java
Simulation of a simple game (no iteration, only one play-through per execution).  

The game is using two normal six sided dice.  
You play against the computer and the objective is to get as close to nine as possible, but not more.

Rules:
- Both you and the computer have two possible dice to roll. 
- First you roll a die and then decide if you would like to roll another.
- When you are done, it is time for the computer to do the same. The computer rolls the first die and if it is four or lower, it will roll another, otherwise it will skip the second roll.
- When both players have done their rolls, the program calculates who, if anyone, won. As stated, the one closest to nine wins, unless it is ten or more. If either player gets ten or more, the player automatically loses.

```
Playing a game
=================

Ready to play? (Y/N) Y
You rolled 3
Would you like to roll again? (Y/N) Y
You rolled 6 and in total you have 9
The computer rolled 3
The computer rolls again and gets 4 in total 7
You won!
```

### DangerousWork.java
Dangerous work with some rather strange payment. For each day you stay at the work, the salary is doubled -- it will begin with 0.01 of a Swedish krona (also called 1 "öre") the first day and it will be 0.02 the second, 0.04 the third, 0.08 the fourth and so on. 
The program calculates the minimum number of days you need to work to earn a specific amount. E.g.:
```
How much would you like to earn? 1000000
You will have your money in 27 days.
```

### Diamonds.java
The program creates a diamond of stars by reading a user input (a positive number which corresponds to the height till the middle of the diamond.). End the program by typing a negative number. E.g.:
```
Give a positive number: 5
    *
   ***
  *****
 *******
*********
 *******
  *****
   ***
    *
    
    
    
Give a positive number: 3
  *
 ***
*****
 ***
  *
Give a positive number: -3
```

### GameSRP.java
Scissor-rock-paper game.  
A scissor can cut paper, a rock kan nock a scissor and a paper can wrap a rock.  

### DayOfWeek.java
Prints day of the Week of given input (Year, Month, Day(1-31)).  
Example:  
```
Enter year: 2015
Enter month (1-12): 1
Enter day of the month (1-31): 25
Day of week is Sunday

Enter year: 2012
Enter month (1-12): 5
Enter day of the month (1-31): 12
Day of week is Saturday

Enter year: 1975
Enter month (1-12): 8
Enter day of the month (1-31): 21
Day of week is Thursday
```

### Ants.java
Simulation of an ant randomly taking steps on a chessboard. It can walk up, down, left and right (not diagonally). The ant cannot walk over the edge of the chess board.  
Each simulation calculates the number of "steps" the ant takes to visit all squares on the chess board. The simulations is done ten times to calculate an average ampunt of steps.

### Hex2Dec.java
Converts Hexadecimals to Decimals.  
Example:  
```
Enter a hex number: af71
The decimal value for af71 is 44913.
```

### PrintCalendar.java
Print calendar based on Year and Month input.  
Example 1:  
```
Enter a year after 1800: 1975
Enter a month (1-12): 8
August 1975
-----------------------------
 Mon Tue Wed Thu Fri Sat Sun
                   1   2   3
   4   5   6   7   8   9  10
  11  12  13  14  15  16  17
  18  19  20  21  22  23  24
  25  26  27  28  29  30  31

```

Example 2:  
```
Enter a year after 1800: 2020
Enter a month (1-12): 11
November 2020
-----------------------------
 Mon Tue Wed Thu Fri Sat Sun
                           1
   2   3   4   5   6   7   8
   9  10  11  12  13  14  15
  16  17  18  19  20  21  22
  23  24  25  26  27  28  29
  30

```

